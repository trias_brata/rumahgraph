var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(function(mix) {
    mix.less('main.less','public/css/admin.css');
    mix.less('app.less','public/css/app.css');
    mix.scripts(
        [
        'canvas/jquery.js',
        'canvas/plugins.js',
        // 'canvas/functions.js',
        // 'canvas/events-data.js',
        // 'canvas/jquery.calendario.js',
        // 'canvas/jquery.camera.js',
        // 'canvas/jquery.elastic.js',
        // 'canvas/jquery.gmap.js',
        // 'canvas/jquery.mousewheel.min.js',
        // 'canvas/jquery.nivo.js',
        // 'canvas/jquery.vmap.js',
        // 'canvas/canvas.slider.fade.js',
        ],
        'public/js/app.js'
    );
    mix.scripts([
        'map.api.js',
        '../vendor/jquery/dist/jquery.js',
        'vendor/bootstrap/bootstrap.min.js',
        'vendor/raphael/raphael-min.js',
        'vendor/morris.js/morris.min.js',
        'vendor/moment/moment.min.js',
        'vendor/bootstrap-datepicker/bootstrap-datepicker.js',
        'vendor/bootstrap-datepicker/locales/bootstrap-datepicker.id.js',
        'vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
        'vendor/wizard/jquery.bootstrap.wizard.min.js',
        'vendor/typeahead/bloodhound.min.js',
        'vendor/inputmask/jquery.inputmask.bundle.min.js',
        'vendor/typeahead/typeahead.bundle.min.js',
        'vendor/typeahead/typeahead.jquery.min.js',
        '../vendor/summernote/dist/summernote.min.js',
        '../vendor/select2/dist/js/select2.min.js',
        'vendor/spin.js/spin.min.js',
        'vendor/autosize/jquery.autosize.min.js',
        'vendor/flot/jquery.flot.min.js',
        'vendor/flot/jquery.flot.time.min.js',
        'vendor/flot/jquery.flot.pie.min.js',
        'vendor/flot/jquery.flot.resize.min.js',
        'vendor/flot/jquery.flot.orderBars.js',
        'vendor/flot/curvedLines.js',
        'vendor/skycons/skycons.js',
        'vendor/d3/d3.min.js',
        'vendor/d3/d3.layout.min.js',
        'vendor/d3/d3.v3.js',
        'vendor/rickshaw/rickshaw.js',
        'vendor/sparkline/jquery.sparkline.min.js',
        'vendor/nanoscroller/jquery.nanoscroller.min.js',
        'vendor/nanoscroller/overthrow.min.js',
        'vendor/toastr/toastr.js',
        'vendor/DataTables/jquery.dataTables.min.js',
        'vendor/jasny-bootstrap/jasny-bootstrap.min.js',
        'vendor/dropzone/dropzone.js',
        '../vendor/cropit/dist/jquery.cropit.js',
        '../vendor/gmaps/gmaps.min.js',
      
        /*core js*/
        // 'vendor/gmaps/gmaps.js',
        'core/source/App.min.js',
        'core/source/AppCard.js',
        'core/source/AppForm.js',
        'core/source/AppNavSearch.js',
        'core/source/AppNavigation.js',
        'core/source/AppVendor.js',
          /**
         * own resorce
         */
        
        'app.js',

        ],'public/js/admin.js');
    mix.copy('resources/assets/vendor/font-awesome/fonts','public/fonts/');
    mix.copy('resources/assets/vendor/summernote/dist/font','public/fonts/summernote');
    mix.copy('resources/assets/vendor/roboto-fontface/fonts','public/fonts/roboto-fontface/fonts');
    mix.copy('resources/assets/vendor/open-sans-fontface/fonts','public/fonts/open-sans-fontface/');
    mix.copy('resources/assets/vendor/material-design-iconic-font/fonts','public/fonts/material-design-iconic-font/fonts/');
    mix.copy('resources/assets/vendor/canvas/fonts','public/fonts/canvas/');
    mix.copy('resources/assets/fonts/font-google','public/fonts/font-google/');
});
