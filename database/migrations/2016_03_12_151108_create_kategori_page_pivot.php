<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKategoriPagePivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori_page', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kategori_id',false,true);
            $table->foreign('kategori_id')->references('id')->on('kategoris')->onDelete('cascade');
            $table->integer('page_id',false,true);
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kategori_page');
    }
}
