@extends('layouts.public')
@section('body-class','class="stretched"')
@section('content')
<section id="content">
    <div class="content-wrap nopadding">
        <div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: #eee;"></div>

        <div class="section nobg full-screen nopadding nomargin">
            <div class="container vertical-middle divcenter clearfix">

                <div class="row center">
                    <a href="{{ url('/') }}"><img src="{{ asset('imgs/sm-logo.png') }}" alt="Rumah Graph Logo"></a> 
                </div>
                    @if (Session::has('errors'))
                        <div class="divcenter noradius noborder" style="max-width:400px;padding-top: 15px;">
                            @include('incl.error')
                        </div>
                    @endif
                <div class="panel panel-default divcenter noradius noborder" style="max-width: 400px;">

                    <div class="panel-body" style="padding: 40px;">
                        {!! Form::open(['action'=>'Auth\AuthController@login','id'=>'login-form','name'=>'login-form','class'=>'nobottommargin']) !!}
                        <h3>Login to your Account</h3>

                        <div class="col_full">
                            {!! Form::label('email','E-Mail:')!!}
                            {!! Form::text('email',null,['class'=>'form-control not-dark','id'=>'email'])!!}
                        </div>

                        <div class="col_full">
                            {!! Form::label('password','password:')!!}
                            {!! Form::password('password',['class'=>'form-control not-dark','id'=>'password'])!!}
                        </div>

                        <div class="col_full nobottommargin">
                            <button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
                            <a href="{{ url('password/reset/') }}" class="fright">Forgot Password?</a>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
                <!-- <div class="clearfix"></div> -->
                <div class="row center dark"><small>Copyrights &copy; All Rights Reserved by Rumah Graph.</small></div>
            </div>
        </div>

    </div>

</section><!-- #content end -->
    <!-- Go To Top
    ============================================= -->
    @endsection
    @section('additional')
    <div id="gotoTop" class="icon-angle-up"></div>
    @stop
