<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Rumah Graph |</title>
	<!-- <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" /> -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}"/>
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
	

    <!--[if lt IE 9]>
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
</head>
	<body @yield('body-class')>
	<div id="wrapper" class="clearfix">
		@yield('content')
	</body>
	@yield('additional')
	<script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
	
</html>