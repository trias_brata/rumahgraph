<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Rumah Graph</title>
	<!-- <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" /> -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}"/>
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
	<meta property="og:url"           content="{{url()->full()}}" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Rumah Graph" />
	<meta property="og:locale"         content="ID_id" />
	<meta property="og:description"   content="Rumah Graph is place for creative people" />
	<meta property="og:image"         content="{{ asset('imgs/logo.png') }}" />
	<meta property="fb:app_id" content="1702816419938900">
	

    <!--[if lt IE 9]>
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
</head>
	<body class="stretched">
	<div id="wrapper" class="clearfix">
	<div id="fb-root"></div>
	<script> window.fbAsyncInit = function() {FB.init({appId      : '1702816419938900', xfbml      : true,status : true, cookie : true, version    : 'v2.5'}); }; (function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk')); $(document).on('click','.si-twitter',function() {width  = 575, height = 400, left   = ($(window).width()  - width)  / 2, top    = ($(window).height() - height) / 2, url    = this.href, opts   = 'status=1' + ',width='  + width  + ',height=' + height + ',top='    + top    + ',left='   + left; window.open(url, 'twitter', opts); return false }); $(document).on('click','.si-facebook',function (e) {console.log(e); FB.ui({method: 'share_open_graph', action_type: 'og.likes', action_properties: JSON.stringify({object:'https://developers.facebook.com/docs/', }) }); }); </script>
		 @include('incl.public.menu')
		@yield('content')
		@include('incl.public.footer')
		<div id="gotoTop" class="icon-angle-up"></div>

	
	@yield('additional')
	<script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
	</body>

</html>