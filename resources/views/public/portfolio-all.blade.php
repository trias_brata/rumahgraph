@extends('layouts.public-main')
@section('content')
<section id="page-title">

    <div class="container clearfix">
        <h1>Project</h1>
        <span>Showcase of Our Awesome Works in 4 Columns</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Portfolio</li>
        </ol>
    </div>

</section>
<section id="content">
    <div class="content-wrap nobottompadding">
        <div class="container clearfix">
            <ul id="portfolio-filter" class="clearfix">

                <li class="activeFilter"><a href="#" data-filter="*">Show All</a></li>
                @foreach ($kategori as $el)
                    <li><a href="#" data-filter=".pf-{{str_replace(' ','-',strtolower($el->title))}}">{{$el->title}}</a></li>
                @endforeach
            </ul>
            <div id="portfolio-shuffle">
                <i class="icon-random"></i>
            </div>

        </div>
        <div id="portfolio" class="portfolio-nomargin portfolio-notitle portfolio-full clearfix">
            @foreach ($produk as $prdk)
            <?php 
            $kat = [];
            foreach ($prdk->kategori as $temp) {
                $tmpnm = str_replace(' ','-',strtolower($temp->title));
               $kat[] = "pf-$tmpnm"; 
           } 
           $kat = implode(' ',$kat);
           ?> 
           <article class="portfolio-item {{$kat}}">
            
                <div class="portfolio-image">
                    @if ($prdk->preview_type == 'image')
                    <a href="{{ route('portfolio',$prdk->id) }}">
                        <img src="{{ asset('imgs/project/thumbs/'.$prdk->preview) }}" alt="{{$prdk->nama}}">
                    </a>
                    <div class="portfolio-overlay">
                        <a href="{{ asset('imgs/project/'.$prdk->preview) }}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                        <a href="{{ route('portfolio',$prdk->id) }}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                    </div>
                    @else
                     <a href="{{ route('portfolio',$prdk->id)}}">
                        <img src="{{ asset('imgs/project/thumbs/'.$prdk->preview.'.jpeg') }}" alt="{{$prdk->nama}}">
                    </a>
                    <div class="portfolio-overlay">
                        <a href="http://www.youtube.com/watch?v={{$prdk->preview}}" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
                        <a href="{{ route('portfolio',$prdk->id) }}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                    </div>
                    @endif
                </div>
                <div class="portfolio-desc">
                    <h3><a href="{{ route('portfolio',$prdk->id) }}">{{$prdk->nama}}</a></h3>
                    <span>
                    <?php $x=0; ?>
                    @foreach ($prdk->kategori as $e)
                        <a href="#">{{$e->title}}</a>
                        <?php if($prdk->kategori->count() > $x+1 ) echo ","; $x++; ?>
                    @endforeach
                    </span>
                </div>
        </article>
        @endforeach


    </div><!-- #portfolio end -->

<!-- Portfolio Script
    ============================================= -->
    <script type="text/javascript">

        jQuery(window).load(function(){

            var $container = $('#portfolio');

            $container.isotope({ transitionDuration: '0.65s' });

            $('#portfolio-filter a').click(function(){
                $('#portfolio-filter li').removeClass('activeFilter');
                $(this).parent('li').addClass('activeFilter');
                var selector = $(this).attr('data-filter');
                $container.isotope({ filter: selector });
                return false;
            });

            $('#portfolio-shuffle').click(function(){
                $container.isotope('updateSortData').isotope({
                    sortBy: 'random'
                });
            });

            $(window).resize(function() {
                $container.isotope('layout');
            });

        });

    </script><!-- Portfolio Script End -->

</div>

</section>
@stop