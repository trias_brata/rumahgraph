@extends('layouts.public-main')
@section('content')
<section id="page-title">
    <div class="container clearfix">
        <h1>Artikel</h1>
        <span>Artikel Dari Rumah Graph</span>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="active">Artikel</li>
        </ol>
    </div>
</section>
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">


            <div class="postcontent nobottommargin clearfix">


                <div id="posts" class="post-timeline clearfix">

                    <div class="timeline-border"></div>
                    @forelse ($page as $blog)
                    <div class="entry clearfix">
                        <div class="entry-image">
                            <a data-lightbox="image" href="{{ asset('imgs/article/'.$blog->picture) }}"><img alt="{{$blog->title}}" src="{{ asset('imgs/article/'.$blog->picture) }}" class="image_fade" style="opacity: 1;"></a>
                        </div>
                        <div class="entry-timeline">
                            {!! $blog->formated_date !!}
                            <div class="timeline-divider"></div>
                        </div>
                        <div class="entry-title">
                            <h2><a href="{{ route('article',$blog->slug) }}">{{$blog->title}}</a></h2>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li><a href="#"><i class="icon-user"></i>{{$blog->user->name}}</a></li>
                            <li><i class="icon-folder-open"></i>
                                @foreach ($blog->kategori as $kat)
                                <a href="{{route('tag',$kat->slug)}}">{{$kat->title}}</a>,   
                                @endforeach
                            </li>
                        </ul>
                        <div class="entry-content">
                            {!!explode('<hr class="read-more">', $blog->text)[0]!!}
                        </div>
                    </div>
                    @empty

                    @endforelse
                    @include('pagination', array('paginator' => $page))
                </div><!-- #posts end -->

            </div><!-- .postcontent end -->

                        <!-- Sidebar
                        ============================================= -->
                        <div class="sidebar nobottommargin col_last clearfix">
                            <div class="sidebar-widgets-wrap">

                                <div class="widget widget-twitter-feed clearfix">

                                    <h4>Twitter Feed</h4>
                                    <ul id="sidebar-twitter-list-1" class="iconlist">
                                        <li></li>
                                    </ul>

                                    <a href="" class="btn btn-default btn-sm fright">Follow Us on Twitter</a>

                                    <script type="text/javascript">
                                        jQuery( function($){
                                            $.getJSON('include/twitter/tweets.php?username=envato', function(tweets){
                                                $("#sidebar-twitter-list-1").html(sm_format_twitter(tweets));
                                            });
                                        });
                                    </script>

                                </div>
                                <div class="widget clearfix">

                                    <h4>Portfolio Kami</h4>
                                    <div id="oc-portfolio-sidebar" class="owl-carousel portfolio-5">
                                        @foreach ($produk as $product)
                                        <div class="oc-item">
                                            <div class="iportfolio">
                                                <div class="portfolio-image">
                                                    @if ($product->preview_type == "image")
                                                    <a href="{{ route('portfolio',$product->id) }}">
                                                        <img src="{{ asset("imgs/project/thumbs/{$product->preview}") }}" alt="{{$product->nama}}">
                                                    </a>
                                                    <div class="portfolio-overlay">
                                                        <a href="{{ asset("imgs/project/{$product->preview}") }}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                                        <a href="{{ route('portfolio',$product->id) }}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                                    </div>
                                                    @else
                                                    <a href="{{ route('portfolio',$product->id) }}">
                                                        <img src="{{ asset("imgs/project/thumbs/{$product->preview}.jpeg") }}" width="" alt="{{$product->nama}}">
                                                    </a>
                                                    <div class="portfolio-overlay">
                                                        <a href="https://www.youtube.com/watch?v={{$product->preview}}" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
                                                        <a href="{{ route('portfolio',$product->id) }}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                                    </div>
                                                    @endif
                                                </div>
                                                <div class="portfolio-desc">
                                                    <h3><a href="{{ route('portfolio',$product->id) }}">{{$product->nama}}</a></h3>
                                                    <span>
                                                        <?php $all = [];?>
                                                        @foreach ($product->kategori->take(2) as $k)
                                                        <?php $all[] ="<a href='#'>{$k->title}</a>" ?>
                                                        @endforeach
                                                        {!! implode(', ', $all) !!}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>

                                    <script type="text/javascript">

                                        jQuery(document).ready(function($) {

                                            var ocClients = $("#oc-portfolio-sidebar");

                                            ocClients.owlCarousel({
                                                items: 1,
                                                margin: 10,
                                                loop: true,
                                                nav: false,
                                                autoplay: true,
                                                dots: true,
                                                autoplayHoverPause: true
                                            });

                                        });

                                    </script>

                                </div>

                                <div class="widget clearfix">

                                    <h4>Kategori Artikel</h4>
                                    <div class="tagcloud">
                                        @foreach ($kategori as $el)
                                        <a href="{{route('tag',$el->slug)}}">{{$el->title}}</a>
                                        @endforeach
                                    </div>

                                </div>

                            </div>

                        </div><!-- .sidebar end -->

                    </div>

                </div>

            </section>      
            @stop