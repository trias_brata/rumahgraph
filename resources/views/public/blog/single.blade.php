@extends('layouts.public-main')
@section('content')
<section id="page-title">

	<div class="container clearfix">
		<h1>Baca Artikel</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/') }}">Home</a></li>
			<li><a href="{{ route('article') }}">Artikel</a></li>
			<li class="active">{{$page->title}}</li>
		</ol>
	</div>

</section><!-- #page-title end -->

        <!-- Content
        ============================================= -->
        <section id="content">
        	<div class="content-wrap">
        		<div class="container clearfix">
        			<div class="postcontent nobottommargin clearfix">
        				<div class="single-post nobottommargin">
        					<div class="entry clearfix">
                                <div class="entry-image">
                                    <a href="#"><img src="{{ asset('imgs/article/'.$page->picture) }}" alt="{{$page->title}}"></a>
                                </div>
                                <div class="entry-title">
                                 <h2>{{$page->title}}</h2>
                             </div>
                             <ul class="entry-meta clearfix">
                                 <li><i class="icon-calendar3"></i> {{$page->formated_date_full}}</li>
                                 <li><a href="#"><i class="icon-user"></i>{{$page->user->name}}</a></li>
                                 <li><i class="icon-folder-open"></i>
                                    <?php $x=0?>
                                    @foreach ($page->kategori->take(2) as $k)
                                    <a href="{{ route('tag',$k->slug) }}">{{$k->title}}</a>
                                    <?php if($x<1) echo ",";$x++?>
                                    @endforeach
                                </li>
                            </ul>
                            <div class="entry-content notopmargin">
                             {!! str_replace('<hr class="read-more">', '', $page->text) !!}
                             <div class="tagcloud clearfix bottommargin">
                                @foreach ($page->kategori as $k)
                                <a href="{{ route('tag',$k->slug) }}">{{$k->title}}</a>
                                @endforeach
                            </div>
                            <div class="clear"></div>
                            <div class="si-share noborder clearfix">
                                <span>Bagikan Artikel:</span>
                                <div>
                                   <a class="social-icon si-borderless si-facebook fb-share-button" data-href="{{ route('article',$page->id) }}">
                                      <i class="icon-facebook"></i>
                                      <i class="icon-facebook"></i>
                                  </a>
                                  <a  href="https://twitter.com/intent/tweet?url={{urlencode(route('article',$page->id))}}&text={{urlencode("Artikel Rumah Graph {$page->title}")}}&via=rumahGrpah&lang=id&hashtags=rumahgraph" class="social-icon si-borderless si-twitter">
                                      <i class="icon-twitter"></i>
                                      <i class="icon-twitter"></i>
                                  </a>
                                  <a href="#" class="social-icon si-borderless si-pinterest">
                                      <i class="icon-pinterest"></i>
                                      <i class="icon-pinterest"></i>
                                  </a>
                                  <a href="#" class="social-icon si-borderless si-gplus">
                                      <i class="icon-gplus"></i>
                                      <i class="icon-gplus"></i>
                                  </a>
                                  <a href="#" class="social-icon si-borderless si-rss">
                                      <i class="icon-rss"></i>
                                      <i class="icon-rss"></i>
                                  </a>
                                  <a href="#" class="social-icon si-borderless si-email3">
                                      <i class="icon-email3"></i>
                                      <i class="icon-email3"></i>
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
                  @if (!is_null($pagination['before']) or !is_null($pagination['after']))
                  <ul class="post-navigation  pager clearfix">
                      <li class="previous nobottommargin">
                         @if (!is_null($pagination['before']))
                         <a href="{{ route('article',$pagination['before']->slug) }}">&larr; {{ $pagination['before']->title }}</a>
                         @else
                         &nbsp;
                         @endif
                     </li>
                     <li class="next  nobottommargin">
                         @if (!is_null($pagination['after']))
                         <a href="{{ route('article',$pagination['after']->slug) }}">{{ $pagination['after']->title }} &rarr; </a>
                         @endif
                     </li>
                 </ul>
                 @endif

                 <div class="line"></div>

                            <!-- Post Author Info
                            ============================================= -->
                            <div class="panel panel-default">
                            	<div class="panel-heading">
                            		<h3 class="panel-title">Ditulis Oleh <span><a href="#">{{ucwords($page->user->name)}}</a></span></h3>
                            	</div>
                            	<div class="panel-body">
                            		<div class="author-image">
                            			<img src="{{ asset('imgs/xs-logo.png') }}" alt="" class="img-circle">
                            		</div>
                            		We Love What we do
                            	</div>
                            </div>
                            @if ($page->page_related->count() > 0)
                            <div class="line"></div>
                            <h4>Artikel Berkaitan:</h4>
                            <div class="related-posts clearfix">
                            	<?php $x=0;?>
                                @foreach ($page->page_related->chunk(2) as $el)
                                <div class="col_half nobottommargin<?php if($x<1) echo "col_last";$x++?>">
                                   @foreach ($el as $article)
                                   <div class="mpost clearfix">
                                      <div class="entry-image">
                                         <a href="{{ route('article',$article->slug) }}"><img src="{{ asset('imgs/article/'.$article->picture) }}" alt="{{$article->title}}"></a>
                                     </div>
                                     <div class="entry-c">
                                         <div class="entry-title">
                                            <h4><a href="#">{{$article->title}}</a></h4>
                                        </div>
                                        <ul class="entry-meta clearfix">
                                            <li><i class="icon-calendar3"></i> {{$article->formated_date_full}}</li>
                                        </ul>
                                        <div class="entry-content">

                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            @endforeach
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @stop