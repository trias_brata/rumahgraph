@extends('layouts.public-main')
@section('content')
@include('incl.public.slider')
        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="promo promo-light promo-full bottommargin-lg header-stick notopborder">
                    <div class="container clearfix">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="text-left visible-md visible-lg">
                                    <h3>Hubungi kami sekarang <span>+62 813 4504 6308</span> atau email kami ke <span>info@rumahgraph.com</span></h3>
                                    <span>Kami sangat ingin sekali membantu setiap usaha untuk mempromosikan usahanya didunia digital</span>  
                                </div>
                                <div class="text-center visible-sm visible-xs">
                                    <h3>Hubungi kami sekarang <span>+62 813 4504 6308</span> atau email kami ke <span>info@rumahgraph.com</span></h3>
                                    <span>Kami sangat ingin sekali membantu setiap usaha untuk mempromosikan usahanya didunia digital</span>  
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-0 col-sm-offset-3 col-sm-6 text-right">
                                <a href="#" class="button button-dark button-xlarge button-rounded" style="margin-top: 0;position: relative;">Hubungi Kami <span class="hidden-md hidden-lg animation zoomOut zoomIn">+62 813 4504 6308</span></a>        
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container clearfix">

                    @foreach ($provide as $prov)
                    <div class="col-md-3 col-sm-6 nobottommargin">
                        <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt noborder fa {{$prov->icon}}"></i></a>
                            </div>
                            <h3>{{$prov->title}}<span class="subtitle">{{$prov->text}}</span></h3>
                        </div>
                        <div class="visible-sm bottommargin-sm"></div>
                    </div>
                    @endforeach
                    <div class="clear"></div>
                    <div class="line bottommargin-lg"></div>

                    @if (!is_null($video))
                    <div class="col-md-4 nobottommargin">
                        <div style="width: 100%;position: relative;">
                            <a href="http://youtube.com/?watch={{$video->link}}" data-lightbox="iframe">
                                <img src="http://img.youtube.com/vi/{{$video->link}}/hqdefault.jpg" style="width: 100%" alt="Image">
                                <div class="i-overlay"><i class="fa fa-play"></i></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 nobottommargin col_last">
                        <div class="heading-block">
                            <h2>{{$video->name}}</h2>
                        </div>
                        {!!$video->describe!!}
                    </div>
                    <div class="clear"></div>
                    @endif
                </div>
                <div class="section topmargin-lg">
                    <div class="container clearfix">

                        <div class="heading-block center">
                            <h2>JASA YANG MEMBUAT MARKET ANDA LEBIH LUAS</h2>
                            <span>Rumah Graph menyediakan berbagai macam jasa yang anda butuhkan untuk menjangkau setiap pelanggan didunia digital</span>
                        </div>

                        <div class="clear bottommargin-sm"></div>
                        @foreach ($jasa as $rowJasa)
                            @foreach ($rowJasa as $col)
                            <div class="col-sm-4" style="margin-bottom: 50px;">
                                <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="fa {{$col->icon}}"></i></a>
                                    </div>
                                    <h3>{{$col->title}}</h3>
                                    <p>{{$col->text}}</p>
                                </div>
                            </div>
                            @endforeach
                            <div class="clear"></div>
                        @endforeach
                    </div>
                </div>

                <div class="container clearfix">

                    <div class="heading-block center">
                        <h3>Beberapa  <span>Portfolio</span> Terbaru</h3>
                        <span>Kami telah bekerja pada beberapa proyek yang mengagumkan yang layak perhitunggkan.</span>
                    </div>

                    <div id="oc-portfolio" class="owl-carousel portfolio-carousel portfolio-nomargin">
                        @foreach ($produk as $product)
                        <div class="oc-item">
                            <div class="iportfolio">
                                <div class="portfolio-image">
                                    @if ($product->preview_type == "image")
                                    <a href="{{ route('portfolio',$product->id) }}">
                                        <img src="{{ asset("imgs/project/thumbs/{$product->preview}") }}" alt="{{$product->nama}}">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="{{ asset("imgs/project/{$product->preview}") }}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                        <a href="{{ route('portfolio',$product->id) }}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                    </div>
                                    @else
                                    <a href="{{ route('portfolio',$product->id) }}">
                                        <img src="{{ asset("imgs/project/thumbs/{$product->preview}.jpeg") }}" width="" alt="{{$product->nama}}">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="https://www.youtube.com/watch?v={{$product->preview}}" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
                                        <a href="{{ route('portfolio',$product->id) }}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                    </div>
                                    @endif
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="{{ route('portfolio',$product->id) }}">{{$product->nama}}</a></h3>
                                    <span>
                                        <?php $all = [];?>
                                        @foreach ($product->kategori->take(2) as $kategori)
                                        <?php $all[] ="<a href='#'>{$kategori->title}</a>" ?>
                                        @endforeach
                                        {!! implode(', ', $all) !!}
                                    </span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <script type="text/javascript">

                        jQuery(document).ready(function($) {

                            var ocPortfolio = $("#oc-portfolio");

                            ocPortfolio.owlCarousel({
                                margin: 1,
                                autoplay: true,
                                autoplayHoverPause: true,
                                dots: false,
                                nav: true,
                                navText : ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
                                responsive:{
                                    0:{ items:1 },
                                    600:{ items:2 },
                                    1000:{ items:3 },
                                    1200:{ items:4 }
                                }
                            });

                        });

                    </script>

                </div>

                <div class="section topmargin-sm nobottommargin">

                    <div class="container clearfix">

                        <div class="heading-block center">
                            <h3>Kata Client</h3>
                            <span>Beberapa pendapat para client yang puas dengan pekerjaan kami</span>
                        </div>

                        <ul class="testimonials-grid grid-3 clearfix nobottommargin">
                            @forelse ($testimonis as $testi)
                            <li>
                                <div class="testimonial">
                                    <div class="testi-image">
                                        <a href="#"><img src="{{ asset('imgs/client/avatar/'.$testi->avatar) }}" alt="{{$testi->name}}"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>{{$testi->say}}</p>
                                        <div class="testi-meta">
                                            {{$testi->name}}
                                            <span>{{$testi->corporate}}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @empty
                            <h4>Testimoni Masi Kosong</h4>
                            @endforelse
                        </ul>


                    </div>

                </div>

               <!--  <div class="container clearfix">

                    <div id="oc-clients" class="owl-carousel owl-carousel-full image-carousel" style="padding: 20px 0;">

                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/1.png" alt="Clients"></a></div>
                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/2.png" alt="Clients"></a></div>
                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/3.png" alt="Clients"></a></div>
                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/4.png" alt="Clients"></a></div>
                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/5.png" alt="Clients"></a></div>
                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/6.png" alt="Clients"></a></div>
                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/7.png" alt="Clients"></a></div>
                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/8.png" alt="Clients"></a></div>
                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/9.png" alt="Clients"></a></div>
                        <div class="oc-item"><a href="http://logofury.com/"><img src="images/clients/10.png" alt="Clients"></a></div>

                    </div>

                    <script type="text/javascript">

                        jQuery(document).ready(function($) {

                            var ocClients = $("#oc-clients");

                            ocClients.owlCarousel({
                                margin: 30,
                                loop: true,
                                nav: false,
                                autoplay: true,
                                dots: false,
                                autoplayHoverPause: true,
                                responsive:{
                                    0:{ items:2 },
                                    480:{ items:3 },
                                    768:{ items:4 },
                                    992:{ items:5 },
                                    1200:{ items:6 }
                                }
                            });

                        });

                    </script>

                </div> -->

                <a class="button button-full  button-dark center tright footer-stick">
                    <div class="container clearfix">
                        Mari kami bantu produk anda menjangkau market digital. <strong>Tertarik</strong> <i class="icon-caret-right" style="top:4px;"></i>
                    </div>
                </a>

            </div>
        </section><!-- #content end -->
        @stop