@extends('layouts.public-main')
@section('content')
<section id="page-title">

    <div class="container clearfix">
        <h1>
            Product {{$produk->nama}} <br>
            <small>for {{$produk->client}}</small>
        </h1>
        <div id="portfolio-navigation">
            @if ($pagination['before'])
            <a href="{{ route('portfolio',$pagination['before']) }}">
                @else
                <a href="#">
                    @endif
                    <i class="icon-angle-left"></i></a>
                    <a href="{{ route('portfolio',null) }}"><i class="icon-line-grid"></i></a>
                    @if ($pagination['after'])
                    <a href="{{ route('portfolio',$pagination['after']) }}">
                        @else
                        <a href="#">
                            @endif
                            <i class="icon-angle-right"></i></a>
                        </div>
                    </div>

                </section>
                <section id="content">

                    <div class="content-wrap">

                        <div class="container clearfix">


                            <div class="col-md-8 portfolio-single-image nobottommargin">
                                @if ($produk->preview_type == 'image')
                                <a href="#"><img src="{{ asset("imgs/project/{$produk->preview}") }}" alt=""></a>
                                @else
                                <iframe  src="https://www.youtube.com/embed/{{$produk->preview}}"  width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                @endif
                            </div>
                            <div class="col-md-4 portfolio-single-content col_last nobottommargin">
                                <div class="fancy-title title-bottom-border">
                                    <h2>Product Info:</h2>
                                </div>
                                {{$produk->describe}}
                                <div class="line"></div>
                                <ul class="portfolio-meta bottommargin">
                                    <li><span><i class="icon-user"></i>Created by:</span> Rumah Graph</li>
                                    <li><span><i class="icon-calendar3"></i>Completed on:</span> {{explode(' ', $produk->created_at)[0]}}</li>
                                    <li><span><i class="icon-tags"></i>Tags:</span> {{implode(', ', $produk->kategori->lists('title')->take(2)->toArray())}}</li>
                                    <li><span><i class="icon-link"></i>Link:</span> <a href="http://{{$produk->link}}">{{$produk->link}}</a></li>
                                </ul>
                                <div class="si-share clearfix">
                                    <span>Bagikan:</span>
                                    <div>
                                        <a class="social-icon si-borderless si-facebook fb-share-button" data-href="{{ route('portfolio',$produk->id) }}">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a  href="https://twitter.com/intent/tweet?url={{urlencode(route('portfolio',$produk->id))}}&text={{urlencode("Produk {$produk->nama} for {$produk->client}")}}&via=rumahGrpah&lang=id&hashtags=rumahgraph" class="social-icon si-borderless si-twitter">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-pinterest">
                                            <i class="icon-pinterest"></i>
                                            <i class="icon-pinterest"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-gplus">
                                            <i class="icon-gplus"></i>
                                            <i class="icon-gplus"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-rss">
                                            <i class="icon-rss"></i>
                                            <i class="icon-rss"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-email3">
                                            <i class="icon-email3"></i>
                                            <i class="icon-email3"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="clear"></div>

                            <div class="divider divider-center"><i class="icon-circle"></i></div>



                            <h4>Related Projects:</h4>

                            <div id="related-portfolio" class="owl-carousel portfolio-carousel">

                                <div class="oc-item">
                                    <div class="iportfolio">
                                        <div class="portfolio-image">
                                            <a href="portfolio-single.html">
                                                <img src="images/portfolio/4/1.jpg" alt="Open Imagination">
                                            </a>
                                            <div class="portfolio-overlay">
                                                <a href="images/portfolio/full/1.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                                <a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                            </div>
                                        </div>
                                        <div class="portfolio-desc">
                                            <h3><a href="portfolio-single.html">Open Imagination</a></h3>
                                            <span><a href="#">Media</a>, <a href="#">Icons</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">

                                jQuery(document).ready(function($) {

                                    var relatedPortfolio = $("#related-portfolio");

                                    relatedPortfolio.owlCarousel({
                                        margin: 20,
                                        nav: false,
                                        dots:true,
                                        autoplay: true,
                                        autoplayHoverPause: true,
                                        responsive:{
                                            0:{ items:1 },
                                            480:{ items:2 },
                                            768:{ items:3 },
                                            992: { items:4 }
                                        }
                                    });

                                });

                            </script>

                        </div>

                    </div>

                </section>
                @stop