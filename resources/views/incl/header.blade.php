<!-- BEGIN HEADER-->
<header id="header" >
	<div class="headerbar">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="headerbar-left">
			<ul class="header-nav header-nav-options">
				<li class="header-nav-brand" >
					<div class="brand-holder">
						<a href="{{ url('/admin/dashboard') }}">
							<img src="{{ asset('imgs/xs-logo.png') }}" alt="Rumah Graph Logo"> 
						</a>
					</div>
				</li>
				<li>
					<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
						<i class="fa fa-bars"></i>
					</a>
				</li>
			</ul>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="headerbar-right">
			<ul class="header-nav header-nav-profile">
				<li class="dropdown">
					<a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
						<div class="img" style="background-image: url('{{ asset('imgs/user/'.Auth::user()->foto) }}')"></div>
						<span class="profile-info">
							{{Auth::user()->name}}
							<small>{{Auth::user()->username}}</small>
							
						</span>
					</a>
					<ul class="dropdown-menu animation-dock">
						<li class="text-primary text-center">Sebagai Admin</li>
						<li class="divider"></li>
						<li><a href="{{ url('logout') }}"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
					</ul><!--end .dropdown-menu -->
				</li>
			</ul><!--end .header-nav-profile -->
		</div><!--end #header-navbar-collapse -->
	</div>
</header>
<!-- END HEADER-->