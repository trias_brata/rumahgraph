@if (Session::has('errors'))
    @foreach (Session::get('errors')->all() as $error)
        <div class="style-msg errormsg">
            <div class="sb-msg"><i class="fa fa-times"></i><strong>Fatal Error!</strong> {{$error}}</div>
        </div>
    @endforeach
@endif