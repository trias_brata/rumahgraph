<footer id="footer" class="dark">

    <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap clearfix">
                    <div class="row">

                        <div class="col-md-8">

                            <div class="col-md-6">

                                <div class="widget clearfix">

                                    <img src="{{ asset('imgs/sm-logo.png') }}" alt="Logo Rumah Graph" class="footer-logo">

                                    <p>Kami percaya <strong>Kepuasan</strong>, <strong>Kepercayaan</strong> &amp; <strong>Fleksibel</strong> adalah bagian dari DNA Kami.</p>

                                    <div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
                                        <address>
                                            <strong>Kantor:</strong><br>
                                            Jln. Anggur No.39 RT.57<br>
                                            Samrinda, ID 75119<br>
                                        </address>
                                        <abbr title="Phone Number"><strong>Phone:</strong></abbr> +62 813 4504 6308<br>
                                        <abbr title="Email Address"><strong>Email:</strong></abbr> info@rumahgraph.com
                                    </div>

                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="widget clearfix">
                                    <h4>Artikel Terbaru</h4>

                                    <div id="post-list-footer">
                                        @forelse ($recent_blog as $blog)
                                        <div class="spost clearfix">
                                            <div class="entry-c">
                                                <div class="entry-title">
                                                    <h4><a href="#">{{$blog->title}}</a></h4>
                                                </div>
                                                <ul class="entry-meta">
                                                    <li>{{$blog->formated_date_full}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                        @empty
                                        <div class="spost clearfix">
                                            <div class="entry-c">
                                                <div class="entry-title">
                                                    <h4><a href="#">Artikel Masi Kosong</a></h4>
                                                </div>
                                            </div>
                                        </div>
                                        @endforelse
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-4 col_last">

                            <div class="widget clearfix" style="margin-bottom: -20px;">

                                <div class="row">

                                    <div class="col-md-6 bottommargin-sm">
                                        <div class="counter counter-small"><span data-from="0" data-to="15065421" data-refresh-interval="80" data-speed="3000" data-comma="true"></span></div>
                                        <h5 class="nobottommargin">Portfolio</h5>
                                    </div>

                                    <div class="col-md-6 bottommargin-sm">
                                        <div class="counter counter-small"><span data-from="0" data-to="18465" data-refresh-interval="50" data-speed="2000" data-comma="true"></span></div>
                                        <h5 class="nobottommargin">Client</h5>
                                    </div>

                                </div>

                            </div>
                            <div class="widget clearfix" style="margin-bottom: -20px;">

                                <div class="row">

                                    <div class="col-md-6 clearfix bottommargin-sm">
                                        <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a href="#"><small style="display: block; margin-top: 3px;"><strong>Like Kami</strong><br>di Facebook</small></a>
                                    </div>
                                    <div class="col-md-6 clearfix">
                                        <a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="#"><small style="display: block; margin-top: 3px;"><strong>Ikuti Kami</strong><br>di Twitter</small></a>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>

                </div><!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        Copyrights &copy; {{date("Y")}} All Rights Reserved by Rumah Graph.<br>
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="#" class="social-icon si-small si-borderless si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-gplus">
                                <i class="icon-gplus"></i>
                                <i class="icon-gplus"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-pinterest">
                                <i class="icon-pinterest"></i>
                                <i class="icon-pinterest"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-vimeo">
                                <i class="icon-vimeo"></i>
                                <i class="icon-vimeo"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-github">
                                <i class="icon-github"></i>
                                <i class="icon-github"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-yahoo">
                                <i class="icon-yahoo"></i>
                                <i class="icon-yahoo"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-linkedin">
                                <i class="icon-linkedin"></i>
                                <i class="icon-linkedin"></i>
                            </a>
                        </div>

                        <div class="clear"></div>

                        <i class="icon-envelope2"></i> info@rumahgraph.com <span class="middot">&middot;</span> <i class="fa fa-phone"></i> +62 813 4504 6308
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer>