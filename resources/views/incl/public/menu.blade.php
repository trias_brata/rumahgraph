 <header id="header" class="@yield('header-class','transparent-header')">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger" class="visible-md visible-xs" style="position: relative;display: inline-block;float: left"><i class="icon-reorder"></i></div>
                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="#" class="standard-logo"><img src="{{asset('imgs/xs-logo.png')}}" alt="Logo RumahGraph.com"></a>
                        <a href="#" class="retina-logo"><img src=" {{ asset('imgs/xs-logo.png') }} " alt="Logo RumahGraph.com"></a>
                    </div><!-- #logo end -->
                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <ul>
                            <li class="current"><a href="{{url('/')}}"><div>Home</div></a>
                            <li class="current"><a href="{{ route('portfolio') }}"><div>Portfolio</div></a>
                            <li class="current"><a href="{{route('who-we-are')}}"><div>Who Are We?</div></a>
                            <li class="current"><a href="{{ route('article')}}"><div>Article</div></a>
                            </li>
                        </ul>         
                        <!-- Top Search
                        ============================================= -->
                        <div id="top-search">
                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                            <form action="" method="get">
                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                            </form>
                        </div><!-- #top-search end -->

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->