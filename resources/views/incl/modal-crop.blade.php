<script type="text/javascript">
	$('.upload-image').on('change.bs.fileinput',function () {
		$('#mymodal').modal({keyboard : false, backdrop:'static', });
	 });
	$(document).on('click','#crop',function (e) {
		e.stopPropagation();
		e.stopImmediatePropagation();
		$('#mymodal').modal('hide');
		var imageData = $('#preview_upload').cropit('export');
		console.log(imageData)
		$('#data-avatar').val(imageData);
	});
</script>
<div id="mymodal" class="modal fade">
	<div class="modal-dialog">
		<div class="card box-crop">
			<div class="card-head">
				<header>Crop Image</header>
			</div>
			<div class="card-body no-padding">
				<div class="box-crop-cropit-preview">
					<div class="cropit-preview"></div>
				</div>
			</div>
			<div class="card-actionbar">
				<div class=" card-actionbar-row block-line">
					<div class="block small">
						<i class="fa fa-picture-o"></i>
					</div>
					<div class="block slider">
						<input type="range" class="cropit-image-zoom-input custom " />						
					</div>
					<div class="block big">
						<i class="fa fa-picture-o fa-2x"></i>						
					</div>
					<div class="btn btn-primary pull-right" id="crop"><i class="fa fa-crop"></i> Crop</div>
					<div class="clearfix"></div>

				</div>
			</div>
		</div>
	</div>
</div>