@extends('base')
@section('title', 'Masuk Sistem')
@section('base-content')
	<section class="container login">
	<div class="row">
		<div class="card no-padding col-md-offset-3 col-md-6  col-xs-8 col-xs-offset-2">
			<div class="card-head no-padding">
				<img src="{{ asset('imgs/background1.jpg') }}" width="100%" alt="">
				<span class="card-title" style="text-align: center; width: 100%" >
					@yield("title-img","Masuk Sistem <br><b>Direktori K-UKMK</b>")
				</span>
			</div>
			<div class="card-body">
				@yield('form')
			</div>
		</div>
	</div>
	</section>
@stop