
@extends('main')
@section('content')
<div class="card card-underline">
	<div class="card-head">
		<header>{!! $documentTitle !!}</header>
		<div class="tools">
			<div class="btn-group">
				<a href="{{ route($index) }}" class="btn btn-icon-toggle"><i class="md md-undo"></i></a>
			</div>
		</div>
	</div>
	<div  class="card-body">
		<ul class="list divider-full-bleed">

			<li class="tile">
				<div class="tile-content ink-reaction">
					<div class="tile-text">
						<span>Icon</span>
						<small>
							<i class="fa fa-2 {{$data->icon}}"></i>
						</small>
					</div>
				</div>
			</li>
			<li class="tile">
				<div class="tile-content ink-reaction">
					<div class="tile-text">
						<span> Title</span>
						<small>{{$data->title}}</small>
					</div>
				</div>
			</li>
			<li class="tile">
				<div class="tile-content ink-reaction">
					<div class="tile-text">
						<span> Describe</span>
						<small>{{$data->text}}</small>
					</div>
				</div>
			</li>
			<li class="tile">
				<div class="tile-content ink-reaction">
					<div class="tile-text">
						<span> Data Created at</span>
						<small>{{$data->created_at}}</small>
					</div>
				</div>
			</li>


		</ul>


	</div>
</div>
@stop