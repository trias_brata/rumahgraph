<div class="form-group floating-label">
	{!!Form::text('nama',null,['class'=>'form-control', 'id'=>'nama'])!!}
	{!!Form::label('nama','Title Project')!!}
</div>
<div class="form-group floating-label">
	{!!Form::text('client',null,['class'=>'form-control', 'id'=>'client'])!!}
	{!!Form::label('client','Client Project')!!}
</div>
<div class="form-group">
	{!!Form::textarea('describe',null,['class'=>'form-control', 'id'=>'text','rows'=>'3'])!!}
	{!!Form::label('text','Describe Project')!!}
</div>
<div class="form-group floating-label">
	<div class="input-group">
		<span class="input-group-addon">
		http://
		</span>
		<div class="input-group-content">
			{!!Form::text('link',null,['class'=>'form-control', 'id'=>'link'])!!}
		</div>
	</div>
	
	{!!Form::label('link','Link Project')!!}
</div>
<div class="form-group">
	{!!Form::select('kategori_list[]',$kategori_list,null,['class'=>'form-control', 'id'=>'kategoi_list','multiple'])!!}
	{!!Form::label('kategoi_list','Category Project Type')!!}
</div>
<div class="form-group floating-label">
	{!!Form::select('preview_type',['image'=>'Image','video'=>'Video'],null,['class'=>'form-control', 'id'=>'label'])!!}
	{!!Form::label('label','Preview Project Type')!!}
</div>
<div id="preview_link" class="form-group floating-label">
	<div class="input-group">
		<span class="input-group-addon">
			http://youtube.com/?watch=
		</span>
		<div class="input-group-content">
			{!!Form::text('preview',null,['class'=>'form-control', 'id'=>'preview_link_form'])!!}
		</div>
	</div>
	{!!Form::label('preview_link_form','Video Preview')!!}
</div>
<div class="form-group" id="preview_upload">
{!!Form::label('preview_link_form','Image Preview')!!}
	<div class="fileinput fileinput-new input-group" style=" margin-top: 0px;" data-provides="fileinput">
		<div class="form-control" data-trigger="fileinput">
			<i class="fa fa-file fileinput-exists"></i> <span class="fileinput-filename"></span>
		</div>
		<span class="input-group-addon btn-file btn btn-primary">
			<span class="fileinput-new">Pilih Gambar</span>
			<span class="fileinput-exists">Ubah Gambar</span><input type="file" id="preview_upload_form" name="preview" accept="image/*">
		</span>
		<a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Batal</a>
	</div>	
</div>

<script type="text/javascript">
	(function () {
		console.log($('[name=preview_type]').val())
		if($('[name=preview_type]').val() == 'image'){
			$('#preview_link').fadeOut(0,function () {
				$('#preview_link_form').attr('disabled','disabled');
				$('#preview_upload').fadeIn(0, function() {
					if($('#preview_upload_form').attr('disabled') == 'disabled'){
						$('#preview_upload_form').removeAttribute('disabled');
					}
				});
			});
		}else if($('[name=preview_type]').val() == 'video'){
			$('#preview_upload').fadeOut(0,function () {
				$('#preview_upload_form').attr('disabled','disabled');
				$('#preview_link').fadeIn(0, function() {
					if($('#preview_link_form').attr('disabled') == 'disabled'){
						$('#preview_link_form').removeAttr('disabled');
					}
				});
			});
		}
	})(); 
	$(document).on('change','[name=preview_type]',function (e) {
		console.log('msg')
		if($(this).val() == 'image'){
			$('#preview_link').fadeOut(400,function () {
				$('#preview_link_form').attr('disabled','disabled');
				$('#preview_upload').fadeIn(400, function() {
					if($('#preview_upload_form').attr('disabled') == 'disabled'){
						$('#preview_upload_form').removeAttr('disabled');
					}
				});
			});
		}else if($(this).val() == 'video'){
			$('#preview_upload').fadeOut(400,function () {
				$('#preview_upload_form').attr('disabled','disabled');
				$('#preview_link').fadeIn(400, function() {
					if($('#preview_link_form').attr('disabled') == 'disabled'){
						$('#preview_link_form').removeAttr('disabled');
					}
				});
			});
		}
	})
</script>