
@extends('main')
@section('content')
<div class="card card-underline">
	<div class="card-head">
		<header>{!! $documentTitle !!}</header>
		<div class="tools">
			<div class="btn-group">
				<a href="{{ route($index) }}" class="btn btn-icon-toggle"><i class="md md-undo"></i></a>
			</div>
		</div>
	</div>
	<div  class="card-body">
		<ul class="list divider-full-bleed">
			<li class="tile">
				<div class="tile-content ink-reaction">
					<div class="tile-text">
						<span>Page Title</span>
						<small>{{$data->title}}</small>
					</div>
				</div>
			</li>
			<li class="tile">
				<div class="tile-content ink-reaction">
					<div class="tile-text">
						<span>Slug Link</span>
						<small><a href="{{ url('page/'.$data->slug) }}">{{ url('page/'.$data->slug) }}</a></small>
					</div>
				</div>
			</li>
			<li class="tile">
				<div class="tile-content ink-reaction">
					<div class="tile-text">
						<span>Text</span>
						<small>

							{!! str_replace('<hr class="read-more">', '', $data->text)!!}
						</small>
					</div>
				</div>
			</li>
			<li class="tile">
				<div class="tile-content ink-reaction">
					<div class="tile-text">
						<span>User</span>
						<small>{{$data->user->name}}</small>
					</div>
				</div>
			</li>
			<li class="tile">
				<div class="tile-content ink-reaction">
					<div class="tile-text">
						<span> Data Created at</span>
						<small>{{$data->created_at}}</small>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>
@stop