<div class="form-group floating-label">
	{!!Form::text('title',null,['class'=>'form-control', 'id'=>'title'])!!}
	{!!Form::label('title','Title Page')!!}
</div>
<div class="form-group">
	{!!Form::text('slug',null,['class'=>'form-control', 'id'=>'slug'])!!}
	{!!Form::label('slug','Slug')!!}
</div>
<div class="form-group" id="preview_upload">
{!!Form::label('media','Gambar header')!!}
	<div class="fileinput fileinput-new input-group" style=" margin-top: 0px;" data-provides="fileinput">
		<div class="form-control" data-trigger="fileinput">
			<i class="fa fa-file fileinput-exists"></i> <span class="fileinput-filename"></span>
		</div>
		<span class="input-group-addon btn-file btn btn-primary">
			<span class="fileinput-new">Pilih Gambar Header</span>
			<span class="fileinput-exists">Ganti Gambar Header</span><input type="file" id="media" name="picture" accept="image/*,video/*">
		</span>
		<a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Batal</a>
	</div>	
	<span class="help-block">
		resolusi terbaik adalah 860x400px dan ukuran file dibawah 1MB, file yang diterima hanya jpg,jpeg,dan png
	</span>
</div>
<div class="form-group">
	{!!Form::textarea('text',null,['class'=>'form-control', 'id'=>'text'])!!}
	{!!Form::label('text','Text')!!}
</div>
<div class="form-group">
	{!!Form::select('kategori_lists[]',$kategori_list,null,['class'=>'form-control','multiple', 'id'=>'kategori_list'])!!}
	{!!Form::label('kategori_list','Text')!!}
</div>

<script type="text/javascript">
	$(document).on('keyup','#title',function () {
		var $slug = '';
		var trimmed = $.trim($(this).val());
		$slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
		replace(/-+/g, '-').
		replace(/^-|-$/g, '');
		$("#slug").val($slug.toLowerCase());
	});
</script>