<div class="form-group floating-label">
	{!! Form::text('name',null,['class'=>'form-control','id'=>'name'])!!}
	{!! Form::label('name','Name Client')!!}
</div>
<div class="form-group floating-label">
	{!! Form::label('corporate','Corporate Client')!!}
	{!! Form::text('corporate',null,['class'=>'form-control','id'=>'corporate'])!!}
</div>
<div class="form-group floating-label">
	{!! Form::label('say','Say')!!}
	{!! Form::text('say',null,['class'=>'form-control','id'=>'say'])!!}
</div>
<div id="preview_upload">
	<div class="form-group">
		{!!Form::label('avatar','Avatar Client')!!}
		<div class="fileinput fileinput-new input-group" style=" margin-top: 0px;" data-provides="fileinput">
			<div class="form-control" data-trigger="fileinput">
				<i class="fa fa-file fileinput-exists"></i> <span class="fileinput-filename"></span>
			</div>
			<span class="input-group-addon btn-file btn btn-primary">
				<span class="fileinput-new">Pilih Gambar</span>
				<span class="fileinput-exists">Ubah Gambar</span>
				<input type="file" id="avatar" class="upload-image cropit-image-input" accept="image/*">
				<input type="hidden" id="data-avatar" name="avatar" />
				{!! Form::hidden('avatar','') !!}
			</span>
			<a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Batal</a>
		</div>
	</div>
	@include('incl.modal-crop')	
	<script type="text/javascript">	
		(function () {
			$('#preview_upload').cropit({
				imageBackground: true,
  				imageBackgroundBorderWidth: 50 
			});
		})(); 
	</script>
</div>