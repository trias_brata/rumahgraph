
@extends('main')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card card-underline">
			<div class="card-head">
				<header>{!! $documentTitle !!}</header>
				<div class="tools">
					<div class="btn-group">
						<a href="{{ route($index) }}" class="btn btn-icon-toggle"><i class="md md-undo"></i></a>
					</div>
				</div>
			</div>
			<div  class="card-body">
				<ul class="list divider-full-bleed">

					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Project Title</span>
								<small>{{$data->nama}}</small>
							</div>
						</div>
					</li>
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Client Project</span>
								<small>{{$data->client}}</small>
							</div>
						</div>
					</li>
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Link Project</span>
								<small><a href="http://{{$data->link}}">{{$data->link}}</a></small>
							</div>
						</div>
					</li>
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Describe Project</span>
								<small>{{$data->describe}}</small>
							</div>
						</div>
					</li>
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Kategori Project</span>
								<small>
									<ul class="unstyled">
										@foreach ($data->kategori as $kategori)
											<li>{{$kategori->title}}</li>
										@endforeach
									</ul>
								</small>
							</div>
						</div>
					</li>
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span> Data Created at</span>
								<small>{{$data->created_at}}</small>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card card-underline">
			<div class="card-head">
				<header>Project Preview</header>
			</div>
			<div class="card-body no-padding">
				@if ($data->preview_type == 'image')
					<img src="{{ asset('imgs/project/'.$data->preview) }}" alt="Preview Project {{$data->nama}}" width="100%">
				@elseif ($data->preview_type == 'video')
					<iframe width="100%" style="min-height: 315px;" src="https://www.youtube.com/embed/{{$data->preview}}" frameborder="0" allowfullscreen></iframe>
				@endif
			</div>
		</div>
	</div>
</div>
@stop