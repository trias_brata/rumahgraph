{!! Form::hidden('remember_token',null) !!}
<div class="form-group floating-label">
	{!!Form::text('name',null,['class'=>'form-control', 'id'=>'name'])!!}
	{!!Form::label('name','Name User')!!}
</div>
<div class="form-group floating-label">
	{!!Form::text('email',null,['class'=>'form-control', 'id'=>'email'])!!}
	{!!Form::label('email','Email User')!!}
</div>
<div class="form-group floating-label">
	{!!Form::password('password',['class'=>'form-control', 'id'=>'password'])!!}
	{!!Form::label('password','Password')!!}
</div>
<div class="form-group floating-label">
	{!!Form::password('password_confirmation',['class'=>'form-control', 'id'=>'password_confirmation'])!!}
	{!!Form::label('password_confirmation','Password Confirmation')!!}
</div>