<div class="form-group floating-label">
	{!!Form::select('icon',$icon,null,['class'=>'form-control', 'id'=>'icon'])!!}
	{!!Form::label('icon','Icon Kategori Produk')!!}
</div>	
<div class="form-group floating-label">
	{!!Form::text('title',null,['class'=>'form-control', 'id'=>'title'])!!}
	{!!Form::label('title','Title')!!}
</div>
<div class="form-group">
	{!!Form::textarea('text',null,['class'=>'form-control', 'id'=>'texts','rows'=>'3'])!!}
	{!!Form::label('texts','Describe')!!}
</div>
<script type="text/javascript">
	$(document).ready(function () {
		function formatState (state) {
			if (!state.id) { return state.text; }
			var $state = $(
				'<span><i class="fa ' + state.text + '"></i> ' + state.text + '</span>'
				);
			return $state;
		};

		$("select").select2({
			templateResult: formatState,
			theme: "bootstrap",
			templateSelection:formatState
		});
	});
</script>
