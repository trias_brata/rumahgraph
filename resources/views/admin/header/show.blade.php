
@extends('main')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card card-underline">
			<div class="card-head">
				<header>{!! $documentTitle !!}</header>
				<div class="tools">
					<div class="btn-group">
						<a href="{{ route($index) }}" class="btn btn-icon-toggle"><i class="md md-undo"></i></a>
					</div>
				</div>
			</div>
			<div  class="card-body">
				<ul class="list divider-full-bleed">
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Headline</span>
								<small>{{$data->headline}}</small>
							</div>
						</div>
					</li>
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Describe Headline</span>
								<small>{{$data->headline}}</small>
							</div>
						</div>
					</li>
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Type</span>
								<small>{{ucfirst($data->type)}}</small>
							</div>
						</div>
					</li>
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span> Data Created at</span>
								<small>{{$data->created_at}}</small>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card card-underline">
			<div class="card-head">
				<header>Preview Media</header>
			</div>
			<div  class="card-body">
				@if ($data->type == "image")
					<img src="{{ asset('header/media/'.$data->media) }}" alt="{{ $data->headline or 'default' }}" width="100%">
				@else
					
				@endif
			</div>
		</div>
	</div>
</div>
@stop