<div class="form-group floating-label">
	{!!Form::text('headline',null,['class'=>'form-control', 'id'=>'headline'])!!}
	{!!Form::label('headline','Headline')!!}
</div>
<div class="form-group floating-label">
		{!! Form::text('desc_headline',null,['class'=>'form-control','id'=>'desc_headline'])!!}
		{!! Form::label('desc_headline','Descrition Headline')!!}
</div>
<div class="form-group floating-label">
		
		{!! Form::select('type',['image'=>'Image','video'=>'Video'],null,['class'=>'form-control','id'=>'type'])!!}
		{!! Form::label('type','Type Media')!!}
</div>
<div class="form-group" id="preview_upload">
{!!Form::label('media','Media')!!}
	<div class="fileinput fileinput-new input-group" style=" margin-top: 0px;" data-provides="fileinput">
		<div class="form-control" data-trigger="fileinput">
			<i class="fa fa-file fileinput-exists"></i> <span class="fileinput-filename"></span>
		</div>
		<span class="input-group-addon btn-file btn btn-primary">
			<span class="fileinput-new">Pilih Media</span>
			<span class="fileinput-exists">Ubah Media</span><input type="file" id="media" name="media" accept="image/*,video/*">
		</span>
		<a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Batal</a>
	</div>	
</div>