<div class="row">
	<div class="col-md-12">
			<div class="form-group floating-label">
				{!!Form::text('name',null,['class'=>'form-control', 'id'=>'name'])!!}
				{!!Form::label('name','Nama Video')!!}
			</div>
			<div class="form-group">
				{!!Form::textarea('describe',null,['class'=>'form-control', 'id'=>'text','rows'=>'3'])!!}
				{!!Form::label('text','Descibe Video')!!}
			</div>
			<div class="form-group floating-label">
				<div class="input-group">
					<span class="input-group-addon">
						http://youtube.com/?watch=
					</span>
					<div class="input-group-content">
						{!!Form::text('link',null,['class'=>'form-control', 'id'=>'link'])!!}
					</div>
				</div>
				{!!Form::label('link','Link Video')!!}
			</div>
	</div>
</div>
