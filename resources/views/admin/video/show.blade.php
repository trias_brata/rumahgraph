
@extends('main')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card card-underline">
			<div class="card-head">
				<header>{!! $documentTitle !!}</header>
				<div class="tools">
					<div class="btn-group">
						<a href="{{ route($index) }}" class="btn btn-icon-toggle"><i class="md md-undo"></i></a>
					</div>
				</div>
			</div>
			<div  class="card-body">
				<ul class="list divider-full-bleed">
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Video Name</span>
								<small>
									{{$data->name}}
								</small>
							</div>
						</div>
					</li>
					<li class="tile">
						<div class="tile-content ink-reaction">
							<div class="tile-text">
								<span>Describe Video</span>
								<small>{!! $data->describe  !!}</small>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card card-underline">
			<div class="card-head">
				<header>Previe Video</header>
				<div class="tools">
					<div class="btn-group">
						<a href="{{ route($index) }}" class="btn btn-icon-toggle"><i class="md md-undo"></i></a>
					</div>
				</div>
			</div>
			<div  class="card-body no-padding">
				<iframe width="100%" height="315" src="https://www.youtube.com/embed/{{$data->link}}" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
		
	</div>
</div>
@stop