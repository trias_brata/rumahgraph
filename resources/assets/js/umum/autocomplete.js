﻿$(function()
{
	'use strict';
	var dataKotaArray = $.map(dataKota,function(value,key){
		return {value:value,data:key};
	});
	var dataKategoriArray = $.map(dataKategori,function(value,key){
		return {value:value,data:key};
	});
	$('.kota-autocomplete').autocomplete(
	{
		lookup:dataKotaArray,
		lookupFilter:function(suggestion,originalQuery,queryLowerCase){
				var re = new RegExp('\\b'+ $.Autocomplete.utils.escapeRegExChars(queryLowerCase),'gi');
				return re.test(suggestion.value);
		}
	});
	$('.kategori-autocomplete').autocomplete(
	{
		lookup:dataKategoriArray,
		lookupFilter:function(suggestion,originalQuery,queryLowerCase){
				var re = new RegExp('\\b'+ $.Autocomplete.utils.escapeRegExChars(queryLowerCase),'gi');
				return re.test(suggestion.value);
		}
	});
});
