window.aOnClikcListener = function (event,obj) {

	var chart = function (chartData) {
		var def = {
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			'<td style="padding:0"><b>{point.y}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
	};
		chartData = $.extend(def,chartData);

		console.log(chartData);
		$('#diagram').highcharts(chartData);

	};
	if(typeof event == 'object'){
		var target = $(event.currentTarget);
		$.get(target.data('chart') , function(res) {
			console.log(res.nama,target.data())
			$('#chartName').html(res.nama);
			 if(target.data('chartNumber') != 1 ||target.data('chartNumber') != 2){
			 	def = {
				 		tooltip: {
				 			pointFormat: '{series.name}: <b>Rp {point.y}</b>'
				 		},
				 		plotOptions: {
				 			pie: {
				 				allowPointSelect: true,
				 				cursor: 'pointer',
				 				dataLabels: {
				 					enabled: true,
				 					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
				 					style: {
				 						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
				 					}
				 				}
				 			}
				 		}
				 	};
                     res.chart = $.extend(true,res.chart, def);
			 }
			chart(res.chart);
		},'json');
	}
	else if (! $.isEmptyObject(obj) ){
		chart(obj);
	}

}
$(document).on('click','a[href*="#"]:not([href="#"])',function(e) {
	if( typeof window.aOnClikcListener == 'function'){
		window.aOnClikcListener(e);
	}
  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    if (target.length) {
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 800);
      return false;
    }
  }
});
$(document).ready(function(){
	var owlitem=$(".item-carousel");
	owlitem.owlCarousel({
		navigation:false,pagination:true,items:5,itemsDesktopSmall:[979,3],itemsTablet:[768,3],itemsTabletSmall:[660,2],itemsMobile:[400,1]
	});
	$("#nextItem").click(function(){
		owlitem.trigger('owl.next');

	})
	$("#prevItem").click(function(){
		owlitem.trigger('owl.prev');

	})
	var featuredListSlider=$(".featured-list-slider");
	featuredListSlider.owlCarousel({
		navigation:false,pagination:false,items:5,itemsDesktopSmall:[979,3],itemsTablet:[768,3],itemsTabletSmall:[660,2],itemsMobile:[400,1]
	});
	$(".featured-list-row .next").click(function(){
		featuredListSlider.trigger('owl.next');

	})
	$(".featured-list-row .prev").click(function(){
		featuredListSlider.trigger('owl.prev');

	})
	$('.list-view,#ajaxTabs li a').click(function(e){
		e.preventDefault();
		$('.grid-view,.compact-view').removeClass("active");
		$('.list-view').addClass("active");
		$('.item-list').addClass("make-list");
		$('.item-list').removeClass("make-grid");
		$('.item-list').removeClass("make-compact");
		$('.item-list .add-desc-box').removeClass("col-sm-9");
		$('.item-list .add-desc-box').addClass("col-sm-7");
		$(function(){
			$('.item-list').matchHeight('remove');

		});

	});
	$('.grid-view').click(function(e){
		e.preventDefault();
		$('.list-view,.compact-view').removeClass("active");
		$(this).addClass("active");
		$('.item-list').addClass("make-grid");
		$('.item-list').removeClass("make-list");
		$('.item-list').removeClass("make-compact");
		$('.item-list .add-desc-box').removeClass("col-sm-9");
		$('.item-list .add-desc-box').addClass("col-sm-7");
		$(function(){
			$('.item-list').matchHeight();
			$.fn.matchHeight._apply('.item-list');

		});

	});
	$(function(){
		$('.row-featured .f-category').matchHeight();
		$.fn.matchHeight._apply('.row-featured .f-category');

	});
	$(function(){
		$('.has-equal-div > div').matchHeight();
		$.fn.matchHeight._apply('.row-featured .f-category');

	});
	$('.compact-view').click(function(e){
		e.preventDefault();
		$('.list-view,.grid-view').removeClass("active");
		$(this).addClass("active");
		$('.item-list').addClass("make-compact");
		$('.item-list').removeClass("make-list");
		$('.item-list').removeClass("make-grid");
		$('.item-list .add-desc-box').toggleClass("col-sm-9 col-sm-7");
		$(function(){
			$('.adds-wrapper .item-list').matchHeight('remove');

		});

	});
	$('.long-list').hideMaxListItems({
		'max':8,'speed':500,'moreText':'View More ([COUNT])'
	});
	$(function(){
		$('[data-toggle="tooltip"]').tooltip()
	})
	$(".scrollbar").scroller();
	$("select.selecter").selecter({
		label:"Select An Item"
	});
	$(".selectpicker").selecter({
		customClass:"select-short-by"
	});
	$(window).bind('resize load',function(){
		if($(this).width()<767){
			$('.cat-collapse').collapse('hide');
			$('.cat-collapse').on('shown.bs.collapse',function(){
				$(this).prev('.cat-title').find('.icon-down-open-big').addClass("active-panel");

			});
			$('.cat-collapse').on('hidden.bs.collapse',function(){
				$(this).prev('.cat-title').find('.icon-down-open-big').removeClass("active-panel");

			})
		}else{
			$('.cat-collapse').removeClass('out').addClass('in').css('height','auto');

		}
	});
	$(".tbtn").click(function(){
		$('.themeControll').toggleClass('active')
	})
});
