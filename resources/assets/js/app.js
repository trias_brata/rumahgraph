var dtable;
$(document).ready(function  () {
	window.selec2Object = $('select.form-control').select2({
		theme: "bootstrap",
	});
	enableDatePicker();
	enableInputMask();	
	setTimeout(function  () {
		getMessage();
	},300)
	fndataTableCaller();
	$('#text').summernote({
		height: $('#text').height(),
		dialogsInBody: true,
		placeholder:'Write here..',
		dialogsFade: true,
		toolbar:[
		['style', ['bold', 'italic', 'underline', 'clear']],
		['font', ['strikethrough', 'superscript', 'subscript']],
		['fontsize', ['fontsize']],
		['color', ['color']],
		['para', ['ul', 'ol', 'paragraph']],
		['custome',['readmore']],
		['height', ['height']],
		['table', ['table']],
		['media',['picture','link']],
		['option',['fullscreen','codeview','help']]
		],
		buttons:{
			readmore:function (context) {
				var ui = $.summernote.ui;
			  // create button
			  var button = ui.button({
			  	contents: '<i class="fa fa-ellipsis-h" style="font-size:17px"/>',
			  	tooltip: 'Read More',
			  	click: function () {
			  		var node = document.createElement('hr');
			  		node.className = "read-more";
			  		context.invoke('insertNode', node);
			  	}
			  });

			  return button.render();
			}
		}
	});
});
function fndataTableCaller () {
	if (!$.isFunction($.fn.dataTable)) {
		return;
	}
	if(!  $.fn.DataTable.fnIsDataTable($('.datatable')))
	{
		window.dtable = $('.datatable').dataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
		
		$('.datatable tbody').on('click', 'tr', function() {
			$(this).toggleClass('selected');
		});
	}
}
function getMessage () {
	$('#message').children().each(function  (index,e) {
		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": true,
			"progressBar": true,
			"positionClass": "toast-bottom-left",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "swing",
			"showMethod": "slideDown",
			"hideMethod": "slideUp"
		};
		if($(e).attr('class') == "error")
		{
			message = function  (message) 
			{
				toastr.error(message, '');
			}
		}
		else if($(e).attr('class')== "warning")
		{
			message = function  (message) 
			{
				toastr.warning(message, '');
			}
		}
		else if($(e).attr('class')== "success")
		{
			message = function  (message) 
			{
				toastr.success(message, '');
			}
		}
		else
		{
			message = function  (message) 
			{
				toastr.info(message, '');
			}
		};
		$(e).children().each(function  (index,el) {
			setTimeout(function  () {
				message($(el).text());
			},index*500);
		})
	})
}
function enableDatePicker () {
	if (!$.isFunction($.fn.datepicker)) {
		return;
	}
	$('.txt-datepicker').datepicker({
		language:'id',
		format: "yyyy-mm-dd"
	});
	$('.bulan-picker').datepicker({
		language:'id',
		format:"mm",
		viewMode:1,
		minViewMode: 1,
		maxViewMode: 1
	});
	$('.tahun-picker').datepicker({
		language:'id',
		format:"yyyy",
		viewMode:"years",
		minViewMode: "years"
	});
}
function enableInputMask () {	 
	if (!$.isFunction($.fn.inputmask)) {
		return;
	}
	$(":input").inputmask();
}