<?php 
namespace App\Providers\Contracts;
use Illuminate\Support\Facades\Facade;
/**
* 
*/
class CarbonFacade extends Facade
{
	public static function getFacadeAccessor()
	{
		return 'carbon';
	}
}