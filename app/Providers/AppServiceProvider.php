<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Icon;
use App\Kategori;
use App\Page;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*.form',function ($view)
        {
           $view->with('icon',with(new Icon)->lists())->with('kategori_list',with(new Kategori)->lists('title','id'));
        });
        view()->composer('public.*',function ($view)
        {
            $view->with('recent_blog',Page::limit(4)->orderBy('created_at','desc')->get());
        });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
