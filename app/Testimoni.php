<?php namespace App;

use Illuminate\Database\Eloquent\Model as Model;
use Bitdev\ModuleGenerator\Contracts\ModelInterface;

class Testimoni extends Model implements ModelInterface
{
		protected $guarded = ['id'];
}
