<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Bitdev\ModuleGenerator\Contracts\ModelInterface;

class Video extends Model implements ModelInterface
{
		protected $guarded = ['id'];
}
