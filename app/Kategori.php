<?php namespace App;

use Illuminate\Database\Eloquent\Model as Model;
use Bitdev\ModuleGenerator\Contracts\ModelInterface;

class Kategori extends Model implements ModelInterface
{
		protected $guarded = ['id'];
		public function produk(){
			return $this->belongsToMany(Produk::class);
		}
		public function page(){
			return $this->belongsToMany(Page::class);
		}
}
