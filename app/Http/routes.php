<?php
Route::post('am/order/checkout',function (Illuminate\Http\Request $r)
{
   return $r->all();
});
Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');

// // Registration Routes...
// Route::get('register', 'Auth\AuthController@showRegistrationForm');
// Route::post('register', 'Auth\AuthController@register');

// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');
Route::get('/home', 'HomeController@index');
Route::group(['namespace'=>'Admin','middleware'=>'auth','prefix'=>'admin'],function ()
{
	function dashboard()
	{
		return view('main');
	}
   Route::get('dashboard', ['as'=>'admin.dashboard','uses'=>function ()
      {
      		return dashboard();
      }]);
   Route::get('/', function ()
   {
   		return dashboard();
   });
   Route::resources([
      'provide'=>'ProvideController',
      'jasa'=>'JasaController',
      'header'=>'HeaderController',
      'produk'=>'ProdukController',
      'testimoni'=>'TestimoniController',
      'kategori'=>'KategoriController',
      'page'=>'PageController',
      'user'=>'UserController',
      'video'=>'VideoController'
   ]);
});
Route::get('/',['as'=>'home','uses'=>'FrontContoller@home']);
Route::get('/who-we-are',['as'=>'who-we-are','uses'=>'FrontContoller@aboutus']);
Route::get('/portfolio/{id?}',['as'=>'portfolio','uses'=>'FrontContoller@portfolio']);
Route::get('/article/{id?}',['as'=>'article','uses'=>'FrontContoller@article']);
Route::get('/tag/{slug}',['as'=>'tag','uses'=>'FrontContoller@tag']);
