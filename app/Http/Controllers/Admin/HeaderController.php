<?php

namespace App\Http\Controllers\Admin;

use Bitdev\ModuleGenerator\Http\Controllers\Controller;

use App\Header as Model;
use Illuminate\Container\Container;
use App\Http\Requests\HeaderRequest;
use Bitdev\UploadHelper\UploadHelper;

class HeaderController extends Controller
{
     private $upload;
     function __construct(Model $repo,Container $container, UploadHelper $upload) {
      $upload->setPath('header/media');
      $this->upload = $upload;
     	parent::__construct($repo,$container,'HeaderRequest');
     }
     /**
      * code for create and update data in data store
      * @param Model $model 
      * @param HeaderRequest         $r     RequestHandler
      * @param string               $from  store|update
      */
     public function CreateOrUpdate(Model $model, HeaderRequest $r, $from)
     {
        $this->upload->setUpdate($from == "update");
        $files = ['media'];
        $data = $r->except($files);
        if($this->upload->run($files,$model,$r)){
          foreach ($files as $file) {
            $data[$file] = $this->upload->get($file);
          }
          return $model->fill($data)->save() ? $this->routeAndSuccess($from) : $this->routeBackWithError($form);
        }
        return $this->routeBackWithError($form);
     }
     /**   
      *delete data in data store
      * @param  Model $model [description]
      * @return [type]                      [description]
      */
     public function destroy($id,Model $model)
     {
        $model =  $model->find($id);
        return  $model->delete() ? $this->routeAndSuccess('destroy') : $this->routeBackWithError('destroy');
     }
}
