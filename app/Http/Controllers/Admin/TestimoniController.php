<?php

namespace App\Http\Controllers\Admin;

use Bitdev\ModuleGenerator\Http\Controllers\Controller;

use App\Testimoni as Model;
use App\Http\Requests\TestimoniRequest;
use Illuminate\Foundation\Application;
use UploadHelper;
use Image;
use Intervention\Image\Exception\NotWritableException;
use Intervention\Image\Exception\NotReadableException;

class TestimoniController extends Controller
{
      private $path;
     function __construct(Model $repo, Application $app ) {
      $this->path = 'imgs/client/avatar/';
     	parent::__construct($repo,$app,'TestimoniRequest');
     }
     /**
      * code for create and update data in data store
      * @param Model $model 
      * @param TestimoniRequest         $r     RequestHandler
      * @param string               $from  store|update
      */
     public function CreateOrUpdate(Model $model, TestimoniRequest $r, $from)
     {
      $files = ['avatar'];
      $data = $r->except($files);
      try {
        $base64_str = substr($r->avatar, strpos($r->avatar, ",")+1);
        $image = base64_decode($base64_str);
        $image = Image::make($image);
        if (! strstr($image->mime,'image/')) throw new NotReadableException;
        $ext = str_replace('image/','.',$image->mime);
        $data['avatar'] = $model->avatar ?: md5(date('Y-M-D h:i:s')).$ext;        
        $image->save($this->path.$data['avatar']);
        return $model->fill($data)->save() ? $this->routeAndSuccess($from) : $this->routeBackWithError($from);
      } 
      catch(NotReadableException $e){
        if($from == 'update'){
          return $model->fill($data)->save() ? $this->routeAndSuccess($from) : $this->routeBackWithError($from);
        }
        return $this->routeBackWithError($from);
      }
      catch (NotWritableException $e) {
        return $this->routeBackWithError($from);
      }         
     }
     /**   
      *delete data in data store
      * @param  Model $model [description]
      * @return [type]                      [description]
      */
     public function destroy($id,Model $model)
     {
        $model = $model->find($id);
        return  $model->delete() ? $this->routeAndSuccess('destroy') : $this->routeBackWithError('destroy');
     }
}
