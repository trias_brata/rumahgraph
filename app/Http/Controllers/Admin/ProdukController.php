<?php

namespace App\Http\Controllers\Admin;

use Bitdev\ModuleGenerator\Http\Controllers\Controller;

use App\Produk as Model;
use Illuminate\Container\Container;
use App\Http\Requests\ProdukRequest;
use Bitdev\UploadHelper\UploadHelper;
use Image;

class ProdukController extends Controller
{
     protected $moduleName = "Project";
     private $upload;
     private $path;
     private $pathThumb;
     private $youtubeThumbPath;
     function __construct(Model $repo,Container $container, UploadHelper $upload) {
      $this->path = 'imgs/project/';
      $this->youtubeThumbPath = 'http://img.youtube.com/vi/{{vidID}}/hqdefault.jpg';
      $this->pathThumb = $this->path.'thumbs/';
      $upload->setPath($this->path);
      $this->upload = $upload;
     	parent::__construct($repo,$container,'ProdukRequest');
     }
     /**
      * code for create and update data in data store
      * @param Model $model 
      * @param ProdukRequest         $r     RequestHandler
      * @param string               $from  store|update
      */
     public function CreateOrUpdate(Model $model, ProdukRequest $r, $from)
     {
      $this->upload->setUpdate('update' == $from);
      if($r->preview_type == 'image'){
        $files = ['preview'];
        $data = $r->except(array_merge($files,['kategori_list']));
        if($this->upload->run($files,$model,$r)){
          foreach ($files as $file) {
            $data[$file] = $this->upload->get($file);
            Image::make($this->path.$data[$file])->resize(400,300)->save($this->pathThumb.$data[$file]);
          }
          if($model->fill($data)->save()){
            $model->kategori()->sync($r->kategori_list);
            return $this->routeAndSuccess($from);
          }
        }
      }else if($r->preview_type == 'video'){
        $file = file_get_contents(str_replace('{{vidID}}',$r->preview,$this->youtubeThumbPath));
        $image = Image::make($file)->resize(400,300);
        $ext = str_replace('image/','',$image->mime);
        $image->save($this->pathThumb."{$r->preview}.$ext");
        if($model->fill($r->except('kategori_list'))->save()){
            $model->kategori()->sync($r->kategori_list);
            return $this->routeAndSuccess($from);
          }
      }
      return $this->routeBackWithError($from);
     }
     /**   
      *delete data in data store
      * @param  Model $model [description]
      * @return [type]                      [description]
      */
     public function destroy($id,Model $model)
     {
        $model =  $model->find($id);
        return  $model->delete() ? $this->routeAndSuccess('destroy') : $this->routeBackWithError('destroy');
     }
}
