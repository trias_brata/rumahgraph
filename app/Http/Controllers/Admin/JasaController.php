<?php

namespace App\Http\Controllers\Admin;

use Bitdev\ModuleGenerator\Http\Controllers\Controller;

use App\Jasa as Model;
use Illuminate\Container\Container;
use App\Http\Requests\JasaRequest;

class JasaController extends Controller
{
     function __construct(Model $repo,Container $container) {
     	parent::__construct($repo,$container,'JasaRequest');
     }
     /**
      * code for create and update data in data store
      * @param Model $model 
      * @param JasaRequest         $r     RequestHandler
      * @param string               $from  store|update
      */
     public function CreateOrUpdate(Model $model, JasaRequest $r, $from)
     {
        return $model->fill($r->all())->save() ? $this->routeAndSuccess($from) : $this->routeBackWithError($form);
     }
     /**   
      *delete data in data store
      * @param  Model $model [description]
      * @return [type]                      [description]
      */
     public function destroy($id,Model $model)
     {
        $model =  $model->find($id);
        return  $model->delete() ? $this->routeAndSuccess('destroy') : $this->routeBackWithError('destroy');
     }
}
