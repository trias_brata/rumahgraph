<?php

namespace App\Http\Controllers\Admin;

use Bitdev\ModuleGenerator\Http\Controllers\Controller;

use App\Page as Model;
use Illuminate\Container\Container;
use App\Http\Requests\PageRequest;
use Auth;
use UploadHelper;
use Image;

class PageController extends Controller
{
     function __construct(Model $repo,Container $container) {
     	parent::__construct($repo,$container,'PageRequest');
     }
     /**
      * code for create and update data in data store
      * @param Model $model 
      * @param PageRequest         $r     RequestHandler
      * @param string               $from  store|update
      */
     public function CreateOrUpdate(Model $model, PageRequest $r, $from)
     {
        UploadHelper::setUpdate('update' == $from);
        UploadHelper::setPath($model->pathHeaderImage);
        if(UploadHelper::run(['picture'],$model,$r)){
          $data = $r->except('kategori_lists','picture');
          $data['picture'] = UploadHelper::get('picture');
          if(!is_dir(public_path($model->pathHeaderImageThumb)))app('files')->makeDirectory($model->pathHeaderImageThumb);
          Image::make($model->pathHeaderImage.$data['picture'])->resize(860,400)->save($model->pathHeaderImage.$data['picture']);
          Image::make($model->pathHeaderImage.$data['picture'])->resize(400,300)->save($model->pathHeaderImageThumb.$data['picture']);
          $data['user_id'] = Auth::user()->id;
            if($model->fill($data)->save()){
              if(count($r->kategori_lists) > 0){
                return $model->kategori()->sync($r->kategori_lists) ? $this->routeAndSuccess($from) : $this->routeBackWithError($from);    
              }
              return $this->routeAndSuccess($from);
            }
        }
        $model->delete();
        $this->routeBackWithError($from);    

        
     }
     /**   
      *delete data in data store
      * @param  Model $model [description]
      * @return [type]                      [description]
      */
     public function destroy($id,Model $model)
     {
        $model =  $model->find($id);
        return  $model->delete() ? $this->routeAndSuccess('destroy') : $this->routeBackWithError('destroy');
     }
}
