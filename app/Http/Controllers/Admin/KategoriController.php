<?php

namespace App\Http\Controllers\Admin;

use Bitdev\ModuleGenerator\Http\Controllers\Controller;

use App\Kategori as Model;
use Illuminate\Container\Container;
use App\Http\Requests\KategoriProdukRequest;

class KategoriController extends Controller
{
     function __construct(Model $repo,Container $container) {
     	parent::__construct($repo,$container,'KategoriProdukRequest');
     }
     /**
      * code for create and update data in data store
      * @param Model $model 
      * @param KategoriProdukRequest         $r     RequestHandler
      * @param string               $from  store|update
      */
     public function CreateOrUpdate(Model $model, KategoriProdukRequest $r, $from)
     {
        $slug = snake_case($r->title);
        if($from != 'update'){
          if(!is_null($model->where('slug',$slug)->first()))
            return redirect()->back()->withErrors('Kategori '.$r->title.' sudah ada sebelumnya')->withInput();
        }
        $data = $r->all();
        $data['slug'] = $slug;
        return $model->fill($data)->save() ? $this->routeAndSuccess($from) : $this->routeBackWithError($form);
     }
     /**   
      *delete data in data store
      * @param  Model $model [description]
      * @return [type]                      [description]
      */
     public function destroy($id,Model $model)
     {
        $model =  $model->find($id);
        return  $model->delete() ? $this->routeAndSuccess('destroy') : $this->routeBackWithError('destroy');
     }
}
