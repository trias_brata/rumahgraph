<?php

namespace App\Http\Controllers\Admin;

use Bitdev\ModuleGenerator\Http\Controllers\Controller;

use App\User as Model;
use Illuminate\Container\Container;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Str;

class UserController extends Controller
{
     function __construct(Model $repo,Container $container) {
     	parent::__construct($repo,$container,'UserRequest');
     }
     /**
      * code for create and update data in data store
      * @param Model $model 
      * @param UserRequest         $r     RequestHandler
      * @param string               $from  store|update
      */
     public function CreateOrUpdate(Model $model, UserRequest $r, $from)
     {
        $data = $r->except('password_confirmation','remeber_token');
        $data['password'] = bcrypt($data['password']);
        $data['remember_token'] = Str::random(60);
        return $model->fill($data)->save() ? $this->routeAndSuccess($from) : $this->routeBackWithError($form);
     }
     /**   
      *delete data in data store
      * @param  Model $model [description]
      * @return [type]                      [description]
      */
     public function destroy($id,Model $model)
     {
        $model =  $model->find($id);
        return  $model->delete() ? $this->routeAndSuccess('destroy') : $this->routeBackWithError('destroy');
     }
}
