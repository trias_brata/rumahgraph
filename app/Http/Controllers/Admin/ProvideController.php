<?php

namespace App\Http\Controllers\Admin;

use Bitdev\ModuleGenerator\Http\Controllers\Controller;

use App\Provide as Model;
use Illuminate\Container\Container;
use App\Http\Requests\ProvideRequest;

class ProvideController extends Controller
{
     function __construct(Model $repo,Container $container) {
     	parent::__construct($repo,$container,'ProvideRequest');
     }
     /**
      * code for create and update data in data store
      * @param Model $model 
      * @param ProvideRequest         $r     RequestHandler
      * @param string               $from  store|update
      */
     public function CreateOrUpdate(Model $model, ProvideRequest $r, $from)
     {
        return $model->fill($r->all())->save() ? $this->routeAndSuccess($from) : $this->routeBackWithError($form);
     }
     /**   
      *delete data in data store
      * @param  Model $model [description]
      * @return [type]                      [description]
      */
     public function destroy($id,Model $model)
     {
        $model =  $model->find($id);
        return  $model->delete() ? $this->routeAndSuccess('destroy') : $this->routeBackWithError('destroy');
     }
}
