<?php namespace App\Http\Controllers;
/**
* Front Controller
*/
use App\Header;
use App\Provide;
use App\Produk;
use App\Jasa;
use App\Page;
use App\Video;
use App\Testimoni;
use App\Kategori;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class FrontContoller extends Controller
{
	public function home()
	{
		$header = Header::all();
		$provide = Provide::all();
		$produk = Produk::all();
		$video = Video::first();
		$jasa = Jasa::all()->chunk(3);
		$testimonis = Testimoni::limit(6)->orderBy('created_at','desc')->get();
		return view('public.home',compact('header','provide','produk','jasa','testimonis','video'));
	}
	public function portfolio($id=null)
	{
		try {
			$produk = Produk::with('kategori','kategori.produk')->findOrFail($id);
			$pagination = [
				'before' => is_null($prdk = Produk::where('id','<', $id)->get()->last()) ? null : $prdk->id,
				'after' => is_null($prdk = Produk::where('id','>', $id)->get()->last()) ? null : $prdk->id,
			];
			return view('public.portfolio-single',compact('produk','pagination'));
		} catch (ModelNotFoundException $e) {
			$produk = Produk::with('kategori')->get();
			$kategori = Kategori::all();
			return view('public.portfolio-all',compact('produk','kategori'));
		}
	}
	public function article($id=null)
	{
		$kategori = Kategori::all();
		try {
			$page = Page::with('kategori','kategori.page','user')->where('slug',$id)->firstOrFail();

			$pagination = [
				'before' => is_null($pageba = Page::where('id','<', $page->id)->get()->last()) ? null: $pageba,
				'after' => is_null($pageba = Page::where('id','>', $page->id)->get()->last()) ? null: $pageba,
			];
			return view('public.blog.single',compact('page','pagination'));
		} catch (ModelNotFoundException $e) {
			$page = Page::orderBy('created_at','desc')->paginate(10);
			$produk =Produk::with('kategori')->orderBy('created_at','desc')->limit(2)->get();
			return view('public.blog.index',compact('page','kategori','produk'));
		}
	}
	public function aboutus()
	{
		return view('public/about');
	}
}