<?php

namespace App\Http\Controllers;

use Bitdev\ModuleGenerator\Http\Controllers\Controller;

use NamespaceProject\Model as Model;
use NamespaceProject\Http\Requests\KategoriProdukRequest;

class KategoriProdukController extends Controller
{
     function __construct(Model $repo) {
     	parent::__construct($repo,'KategoriProdukRequest');
     }
     /**
      * code for create and update data in data store
      * @param Model $model 
      * @param KategoriProdukRequest         $r     RequestHandler
      * @param string               $from  store|update
      */
     public function CreateOrUpdate(Model $model, KategoriProdukRequest $r, $from)
     {
        return $model->fill($r->all())->save() ? $this->routeAndSuccess($from) : $this->routeBackWithError($form);
     }
     /**   
      *delete data in data store
      * @param  Model $model [description]
      * @return [type]                      [description]
      */
     public function destroy(Model $model)
     {
        return  $model->delete() ? $this->routeAndSuccess('destroy') : $this->routeBackWithError('destroy');
     }
}
