<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule =[
            'title'=>'required',
            'slug'=>'required',
            'text'=>'required',
            'picture'=>'max:1024|image'
        ];
        if($this->isMethod('post'))
            $rule['slug'].='|unique:pages,slug';
        else
            $rule['slug'].='|unique:pages,slug,'.$this->page.',id';
        return $rule; 
    }
}
