<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class UserRequest extends Request
{
    function __construct(User $user)
    {
        $this->user = $user;
        $this->fail = false;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name'=>'required',
            'email'=>'required|email|unique:users,email'
        ];
        $rules['password']= $this->isMethod('post') ? 'required|confirmed|min:6':'confirmed|min:6';
        try {
            $user = $this->user->where('remember_token',$this->input('remember_token',''))->firstOrFail();
            $rules['email'].=','.$user->id;
        } catch (ModelNotFoundException $e) {
            
        }
        return $rules;
    }
}
