<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TestimoniRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        dd($this->all())
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'name' => 'required',
        'corporate' => 'required',
        'avatar' => 'required',
        'say' => 'required',
        ];
    }
    public function attributes()
    {
        return [
        'name' => 'Name Client',
        'corporate' => 'Corporate Client',
        ];
    }
}
