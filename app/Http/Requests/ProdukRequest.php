<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdukRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nama'=>'required',
            'client'=>'required',
            'describe'=>'required',
            'preview_type'=>'required',
        ];
        if($this->input('preview_type') == 'image'){
            $rules['preview'] = 'mimes:jpeg,jpg,png';
        }else if($this->input('preview_type') == 'video'){
            $rules['preview'] = 'required';
        }

        return $rules;
    }
    public function attributes()
    {
        return[
        'nama' => 'Titile Project'
        ];
    }
}
