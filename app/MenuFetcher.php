<?php 
namespace App;
/**
*  service menu fetcher
*/
use Route;
class MenuFetcher
{
	function __construct()
	{

	}
	public function lists($level)
	{
		$menu = [
			'admin'=>[
					[ 'route'=>'dashboard', 'name'=>'Dashboard Admin', 'icon'=> 'fa fa-home'],
					[ 'route'=>'video.index', 'name'=>'Video', 'icon'=> 'fa fa-youtube-play'],
					[ 'route'=>'provide.index', 'name'=>'Provide', 'icon'=> 'fa fa-line-chart'],
					[ 'route'=>'jasa.index', 'name'=>'Jasa', 'icon'=> 'fa fa-shopping-cart'],
					[ 'route'=>'produk.index', 'name'=>'Produk', 'icon'=> 'fa fa-cubes'],
					[ 'route'=>'kategori.index', 'name'=>'Kategori', 'icon'=> 'fa fa-tags'],
					[ 'route'=>'page.index', 'name'=>'Static Page', 'icon'=> 'fa fa-file-text-o'],
					[ 'route'=>'header.index', 'name'=>'Header', 'icon'=> 'fa fa-image'],
					[ 'route'=>'testimoni.index', 'name'=>'Testimoni Client', 'icon'=> 'fa fa-commenting-o'],
					[ 'route'=>'user.index', 'name'=>'User', 'icon'=> 'fa fa-users'],
			]
		];
		return $menu[$level];
	}
	public function make($menu,$role = null,$prefix = null,$child = false)
	{
		$o = "";
		$lists = is_object($menu) ? $menu->lists($role) : $menu;
		foreach ($lists as $list) {
			if(is_array($list)){
				if(isset($list['inGroup'])){
					$inGroup = $list['inGroup'];
					unset($list['inGroup']);
					$active  = in_array( $list['route'],explode( '.',Route::currentRouteName() ) ) ? 'active expand':'';
					if(!$child){
						$icon = isset($list['icon']) ? "<div class='gui-icon'><i class='{$list['icon']}'></i></div>" : ""; 
						$o.="<li class = 'gui-folder $active'>";
						$o.="<a>$icon<span class='title'>{$list['name']}</span></a>";
					}
					else{
						$icon = isset($list['icon']) ? "<i class='{$list['icon']}'></i>" : ""; 
						$o.="<li class = 'gui-folder $active'>";
						$o.="<a><span class='title'>$icon {$list['name']}</span></a>";	
					}
						$o.="<ul>";
						$oldPrefix = $prefix;
						$prefix = (!is_null($prefix)) ? $prefix.$list['route'] : $list['route'];
						$o.=$this->make($inGroup,$role,$prefix,true);
						$prefix = $oldPrefix;
						$o.="</ul>";
					$o.="</li>";
				}else{
					$routeName = $role;
					$routeName.= !is_null($prefix) ? ".$prefix":".";
					$routeName.= "{$list['route']}";
					$link = route($routeName);
					$o.="<li><a href='$link' >";
					if($child){
						$ic= isset($list['icon']) ? "<i class='{$list['icon']}'></i>" : ""; 
						$o.="<span class='title'>$ic {$list['name']}</span>";
					}else{
						$ic= isset($list['icon']) ? "<div class='gui-icon'><i class='{$list['icon']}'></i></div>" : ""; 
						$o.="$ic<span class='title'> {$list['name']}</span>";

					}
					$o.="</a></li>";
				}
			}
		}
		return $o;

	}
}