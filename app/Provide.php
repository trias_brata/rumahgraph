<?php namespace App;

use Illuminate\Database\Eloquent\Model as Model;
use Bitdev\ModuleGenerator\Contracts\ModelInterface;

class Provide extends Model implements ModelInterface
{
		protected $guarded = ['id'];
}
