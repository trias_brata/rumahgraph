<?php namespace App;

use Illuminate\Database\Eloquent\Model as Model;
use Bitdev\ModuleGenerator\Contracts\ModelInterface;

class Produk extends Model implements ModelInterface
{
		protected $guarded = ['id'];
		public function kategori(){
			return $this->belongsToMany(Kategori::class);
		}
		public function getKategoriListAttribute()
		{
			return $this->kategori->lists('id')->toArray();
		}
}
