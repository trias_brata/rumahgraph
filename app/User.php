<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Bitdev\ModuleGenerator\Contracts\ModelInterface;
class User extends Authenticatable implements ModelInterface
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','remember_token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
