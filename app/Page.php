<?php namespace App;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Support\Collection;
use Bitdev\ModuleGenerator\Contracts\ModelInterface;

class Page extends Model implements ModelInterface
{
		protected $guarded = ['id'];
		public $pathHeaderImage = 'imgs/article/';
		public $pathHeaderImageThumb = 'imgs/article/thumb/';
		public function user(){
			return $this->belongsTo(User::class);
		}
		public function kategori(){
			return $this->belongsToMany(Kategori::class);
		}
		public function getKategoriListsAttribute()
		{
			return $this->kategori->lists('id')->toArray();
		}
		public function getFormatedDateAttribute()
		{
			$carbon = app('carbon');
			$carbonDate = $carbon->createFromFormat('Y-m-d H:i:s',$this->created_at)->format('j  <\sp\a\n>M<\/\sp\a\n>');
			return $carbonDate;
		}
		public function getFormatedDateFullAttribute()
		{
			$carbon = app('carbon');
			$carbonDate = $carbon->createFromFormat('Y-m-d H:i:s',$this->created_at)->format('jS F Y');
			return $carbonDate;
		}
		public function delete()
		{
			$fs = app('files');
			$pic = $this->pathHeaderImage.$this->picture;
			$picthumb = $this->pathHeaderImageThumb.$this->picture;
			if($fs->exsist($pic))$fs->delete($pic);
			if($fs->exsist($picthumb))$fs->delete($picthumb);
			return parent::detele();
		}
		public function getPageRelatedAttribute()
		{
			$collection =[];
			foreach ($this->kategori->take(2) as $kategori) {
				foreach ($kategori->page->take(2) as $page) {
					if($this->id !== $page->id) $collection[] = $page;
				}
			}
			return new Collection($collection);
		}
}
